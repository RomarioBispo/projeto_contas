package com.contas.api.services;

import com.contas.api.domain.Conta;

import java.util.List;

public interface ContaService {

    Conta findContaById(Long id);

    List<Conta> findAllContas();

    Conta saveConta(Conta conta);

    Conta updateConta(Long id, Conta conta);

    void deleteConta(Long id);


}
