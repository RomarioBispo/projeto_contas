package com.contas.api.services;

import com.contas.api.domain.Conta;
import com.contas.api.repositories.ContaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContaServiceImpl implements ContaService {

    private ContaRepository contaRepository;

    public ContaServiceImpl(ContaRepository contaRepository) {
        this.contaRepository = contaRepository;
    }

    @Override
    public Conta findContaById(Long id) {
        return contaRepository.findById(id).get();
    }

    @Override
    public List<Conta> findAllContas() {
        return contaRepository.findAll();
    }

    @Override
    public Conta saveConta(Conta conta) {
        return contaRepository.save(conta);
    }

    @Override
    public Conta updateConta(Long id, Conta conta) {
        Conta acc = contaRepository.findById(id).get();
        acc.setNumero_conta(conta.getNumero_conta());
        acc.setCodigo_banco(conta.getCodigo_banco());
        acc.setAgencia(conta.getAgencia());
        return contaRepository.save(acc);
    }

    @Override
    public void deleteConta(Long id) {
        contaRepository.deleteById(id);
    }


}
