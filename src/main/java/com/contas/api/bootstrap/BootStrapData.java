package com.contas.api.bootstrap;

import com.contas.api.domain.Conta;
import com.contas.api.repositories.ContaRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootStrapData implements CommandLineRunner {
    private ContaRepository contaRepository;

    public BootStrapData(ContaRepository contaRepository) {
        this.contaRepository = contaRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Conta a1 = new Conta();
        a1.setAgencia(054);
        a1.setCodigo_banco(01);
        a1.setNumero_conta(111111);
        contaRepository.save(a1);

        Conta a2 = new Conta();
        a2.setAgencia(055);
        a2.setCodigo_banco(01);
        a2.setNumero_conta(222222);
        contaRepository.save(a2);

        Conta a3 = new Conta();
        a3.setAgencia(056);
        a3.setCodigo_banco(01);
        a3.setNumero_conta(333333);
        contaRepository.save(a3);
    }
}
