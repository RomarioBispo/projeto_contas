package com.contas.api.controllers;

import com.contas.api.domain.Conta;
import com.contas.api.services.ContaService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ContaController.BASE_URL)
public class ContaController {
    public static final String BASE_URL = "/api/v1/contas";
    private final ContaService contaService;

    public ContaController(ContaService contaService) {
        this.contaService = contaService;
    }

    /**
     * Método responsável pela listagem de todas as contas persistidas.
     * @return uma lista de todas as contas.
     */
    @GetMapping
    public List<Conta> getAllAccounts() {
        return contaService.findAllContas();
    }

    /**
     * Método responsável pela listagem de uma conta. Dado o seu respectivo id.
     * @param id representa o id da conta.
     * @return a conta que possui o respectivo id.
     */
    @GetMapping("/{id}")
    public Conta getAccountById(@PathVariable Long id) {
        return contaService.findContaById(id);
    }

    /**
     * Método responsável pela criação de uma conta.
     * @param conta representa um objeto da classe conta.
     * @see Conta
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Conta saveAccount(@RequestBody Conta conta) {
        return contaService.saveConta(conta);
    }

    /**
     * Método responsável por atualizar os dados de uma conta. Dado o respectivo id.
     * @param id representa o id da conta
     * @param conta representa um objeto da classe conta.
     * @return
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Conta updateAccount(@PathVariable Long id, @RequestBody Conta conta) {
        return contaService.updateConta(id, conta);
    }

    /**
     * Deleta uma conta. Dado o seu respectivo id.
     * @param id representa o id da conta.
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAccount(@PathVariable Long id) {
        contaService.deleteConta(id);
    }
}
