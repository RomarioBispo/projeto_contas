# README

Pequena API com o objetivo de reunir conhecimentos acerca do spring framework.

### ESCOPO DO PROJETO

- Foi criada uma API em **Java** utilizando **Spring** com as seguintes operações:

    1. Listar contas;
    2. Consultar uma conta por id;
    3. Criar contas;
    4. Atualizar contas;
    5. Remover contas.

### Informações Importantes

- A tabela de contas contém as seguintes colunas:

    - id
    - agencia
    - numero_conta
    - codigo_banco

- O sistema expõe as seguintes rotas:

    - GET /api/v1/contas
        - Listagem de contas.
    - GET /api/v1/contas/{id}
        - Consulta de conta por id.
    - POST /api/v1/contas
        - Criação de conta
    - PUT /api/v1/contas/{id}
        - Atualização de conta por id.
    - DELETE /api/v1/contas/{id}
        - Remoção de conta por id.

- Com um retorno em JSON.

- Utiliza **PostgreSQL** para a persistência.
